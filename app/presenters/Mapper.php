<?php


namespace App\Presenters;


use Dibi\Connection;
use Nette\DI\Container;

class Mapper {

	private $connection;
	public function __construct(Container $container) {
		$params = $container->getParameters()['dibi'];
		 $this->connection = new Connection([
			 'driver'   => $params['driver'],
			 'host'     => $params['host'],
			 'username' => $params['username'],
			 'password' => $params['password'],
			 'database' => $params['database'],
		 ]);
	}


	public function getNetworks() {
		return $this->connection->query('SELECT * FROM networks')->fetchAll();
	}

	public function getNodes() {
		return $this->connection->query('SELECT * FROM network_has_neighbor')->fetchAll();
	}
}