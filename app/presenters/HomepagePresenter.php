<?php

namespace App\Presenters;

use Nette;


final class HomepagePresenter extends Nette\Application\UI\Presenter {
	private $mapper;
	public function __construct(Mapper $mapper) {
		$this->mapper = $mapper;
	}

	protected function beforeRender() {
		parent::beforeRender();
		$this->template->version = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/version');
	}

	public function actionDefault() {
	}
}
